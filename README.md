Task manager
http://immense-depths-9090.herokuapp.com/


Functional requirements

I want to be able to create/update/delete projects
I want to be able to add tasks to my project
I want to be able to update/delete tasks
I want to be able to prioritise tasks into a project
I want to be able to choose deadline for my task
I want to be able to mark a task as 'done'

Technical requirements

It should be WEB application
○ For the client side must be used: HTML, CSS (any libs as Twitter Bootstrap,
Blueprint ...), JavaScript (any libs as jQuery, Prototype ...)
○ For server side any language as Ruby, PHP, Python, JavaScript, C#, Java ...
It should have client side and server side validation.
It should look like on screens (see attached file ‘rg_test_task_grid.png’).

Additional functionality

It should work like one page WEB application and should use AJAX technology, load
and submit data without reloading a page.
It should have user authentication solution and a user should only have access to their
own projects and tasks.
It should have automated tests for all functionality.



Given tables:
-tasks (id, name, status, project_id)
-projects (id, name)

SQL TASK:

1. get all statuses, not repeating, alphabetically ordered

select DISTINCT status from tasks ORDER BY status asc;

(Task.select(:status).uniq.reorder("status DESC").to_sql => "SELECT DISTINCT status FROM \"tasks\"  ORDER BY status DESC")

2. get the count of all tasks in each project, order by tasks count descending

select projects.id, count(tasks) as task_count from tasks right join projects on (tasks.project_id = projects.id) GROUP BY projects.id ORDER BY task_count ASC;

(Task.select('project_id, COUNT(*) AS counter').group('project_id').order('counter DESC').to_sql
 => "SELECT project_id, COUNT(*) AS counter FROM \"tasks\"  GROUP BY project_id ORDER BY tasks.priority DESC, counter DESC")

3. get the count of all tasks in each project, order by projects names

select projects.name, count(tasks) from tasks right join projects on (tasks.project_id = projects.id) GROUP BY projects.id ORDER BY projects.name ASC;

(Project.joins(:tasks).select('projects.name, count(tasks.id) as counter').group('projects.name').to_sql
 => "SELECT projects.name, count(tasks.id) as counter FROM \"projects\" INNER JOIN \"tasks\" ON \"tasks\".\"project_id\" = \"projects\".\"id\" GROUP BY projects.name")

4. get the tasks for all projects having the name beginning with “N” letter

statuselect projects.name, tasks.name from tasks inner join projects on (tasks.project_id = projects.id) where tasks.name like 'N%';

(Task.joins(:project).where('projects.name like ?', 'N%').to_sql
 => "SELECT \"tasks\".* FROM \"tasks\" INNER JOIN \"projects\" ON \"projects\".\"id\" = \"tasks\".\"project_id\" WHERE (projects.name like 'N%') ORDER BY tasks.priority DESC")

5. get the list of all projects containing the ‘a’ letter in the middle of the name, and show the tasks count near each project. Mention that there can exist projects without tasks and tasks with project_id=NULL

select projects.name, count(tasks) from tasks right join projects on (tasks.project_id = projects.id) where projects.name like '%a%' GROUP BY projects.id;

( Project.where('name like ?', '%a%').joins(:tasks).select('tasks.*, count(tasks.id) as counter').group('projects.name').to_sql
 => "SELECT tasks.*, count(tasks.id) as counter FROM \"projects\" INNER JOIN \"tasks\" ON \"tasks\".\"project_id\" = \"projects\".\"id\" WHERE (name like '%a%') GROUP BY projects.name")

6. get the list of tasks with duplicate names. Order alphabetically

select id, name from tasks where name in (select name from tasks GROUP BY name HAVING count(*) > 1) ORDER BY name;
  select tasks.id, tasks.name from tasks right join (select name from tasks GROUP BY name HAVING count(*) > 1) as dup_tasks on (tasks.name = dup_tasks.name) ORDER BY tasks.name;

(Task.group(:content).having("count(id) > 1").order('content').to_sql
 => "SELECT \"tasks\".* FROM \"tasks\"  GROUP BY content HAVING count(id) > 1 ORDER BY tasks.priority DESC, content")

7. get the list of tasks having several exact matches of both name and status, from the project ‘Garage’. Order by matches count

select tasks.name from tasks right join projects on (tasks.project_id = projects.id) where projects.name = 'Garage' GROUP BY tasks.name, tasks.status HAVING count(tasks) > 1 ORDER BY count(tasks);

(Task.includes(:project).where('projects.name like ?', '%Garage%').group('content, completed').having('count(*) > 1').to_sql
 => "SELECT \"tasks\".* FROM \"tasks\"  WHERE (projects.name like '%Garage%') GROUP BY content, completed HAVING count(*) > 1 ORDER BY tasks.priority DESC")

8. get the list of project names having more than 10 tasks in status ‘completed’. Order by project_id

select projects.name from tasks right join projects on (tasks.project_id = projects.id) where tasks.status = 'completed' GROUP BY projects.id HAVING count(tasks) > 10 ORDER BY projects.id;

(Task.includes(:project).where('projects.name like ?', '%Garage%').group('content, completed').having('count(*) > 1').to_sql
 => "SELECT \"tasks\".* FROM \"tasks\"  WHERE (projects.name like '%Garage%') GROUP BY content, completed HAVING count(*) > 1 ORDER BY tasks.priority DESC" 
1.9.3-p194 :012 > Project.joins(:tasks).select('projects.id, tasks.*, count(projects.id) as counter').group('projects.id').having('tasks.completed = ? AND counter > ?', true, 10).to_sql
 => "SELECT projects.id, tasks.*, count(projects.id) as counter FROM \"projects\" INNER JOIN \"tasks\" ON \"tasks\".\"project_id\" = \"projects\".\"id\" GROUP BY projects.id HAVING tasks.completed = 't' AND counter > 10")
