class User < ActiveRecord::Base
  attr_accessible :email, :name, :password, :password_confirmation
  has_secure_password
  validates :password, :presence => {:on => :create}
  validates :name, :presence => true;
  validates :name, :format => { :with => /^([A-z]\s?){3,16}$/i,
      :message => "Only letters and whitespace are allowed in Name" }
  validates :name, :uniqueness=> {:case_sensitive => false, :message => "name is already taken"}
  validates :email, :presence => true;
  validates :email, :format => { :with => /^([A-z0-9\.-_]{2,16})@([A-z0-9\.-_]{2,14})\.([A-z]{2,4})$/i,
                                 :message => "Please, write your real email as email@example.com" }
  validates :email, :uniqueness=> {:case_sensitive => false, :message => "email is already taken"}
  before_save { |user| user.email = email.downcase}
  before_save :create_remember_token
  validates :password_confirmation, :presence => true;
  validates :password, :format => { :with => /^[A-z0-9\!\@\$\%]{6,18}$/i,
      :message => "Only letters, digits and !,@,$,% with length:6-18 symbols are allowed in Password" }
  has_many :projects, dependent: :destroy


  private
    def create_remember_token
      self.remember_token = SecureRandom.urlsafe_base64
    end
end
