class Task < ActiveRecord::Base
  attr_accessible :name, :project_id, :status, :priority, :end_time
  belongs_to :project
  default_scope order: 'tasks.priority DESC'
  validates :project_id, presence: true
  validates :name, presence: true, length: {maximum: 100}
  #validates :priority, length: { maximum: 3 }
end
