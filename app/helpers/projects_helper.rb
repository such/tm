module ProjectsHelper

  def current_project= project
      @current_project = project
  end

  def current_project
    @current_project
  end
end
