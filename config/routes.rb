Tm::Application.routes.draw do
  get "users/new"

  #get "pages/home"
  #root :to =>'pages#home'
  #match '/users/new', to: signup_path
 # match '/tasks', to: 'tasks#index'
 # match '/users', to: 'users#index'
 # match '/projects', to: 'projects#index'
 # match '/', to: 'pages#home'
  resources :tasks
  resources :projects
  resources :sessions, only: [:new, :create, :destroy]
  resources :users
  match '/signup', to: 'users#new'
  match '/signin', to: 'sessions#new'
  match '/signout', to: 'sessions#destroy'
  match '/usershow', to: 'users#show'
  match '/tasks/new/:id', to: 'tasks#new'
  match '/tasks/plus/:id', to: 'tasks#plus'
  match '/tasks/minus/:id', to: 'tasks#minus'
  root :to => 'sessions#home'
  #get "users/new"
  #resources :users do
  #  member do
  #    get 'signup_path'
  #
  #  end
  #end
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.


  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
