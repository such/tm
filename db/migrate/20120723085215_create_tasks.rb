class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.integer :status
      t.integer :project_id
      t.integer :priority
      t.date :end_time

      t.timestamps
    end
  end
end
